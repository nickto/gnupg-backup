# GPG backup CLI

A simple Python wrapper around CLI GnuPG to backup your GPG keys to a file, or
restore them from it.

## Example usage

Backing up your keys to a file:

```bash
gpg-backup backup
```

creates a `gpg_backup_20240209_123807.tar.gpg`.

Backing up your keys to a file without encryption:

```bash
gpg-backup backup --no-encrypt
```

creates a `gpg_backup_20240209_123807.tar`.

Importing your keys from a previously exported file:

```bash
gpg-backup restore gpg_backup_20240209_123807.tar.gpg
```