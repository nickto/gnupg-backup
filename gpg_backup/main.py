import shutil
import subprocess
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import List, Optional

import rich
import typer
from typing_extensions import Annotated

from gpg_backup.constants import OWNER_TRUST, PRIVATE_KEYS, PUBLIC_KEYS
from gpg_backup.gpg_export import (
    compute_hashes,
    create_tar,
    encrypt_file,
    export_private_keys,
    export_public_keys,
    export_trust_database,
    get_default_out_path,
)
from gpg_backup.gpg_import import (
    check_hashes,
    decrypt,
    import_keys,
    import_trust_database,
    is_encrypted,
    unpack,
)
from gpg_backup.pretty import format_as_path, format_as_paths

app = typer.Typer()


def get_gpg_command(gpg: str | None = None, verbose: int = 0) -> str:
    """
    Check if GPG is installed and return the command.
    """
    if gpg is None:
        paths = ["gpg", "gpg2"]
    else:
        paths = [gpg]
    if verbose > 0:
        rich.print(
            f"Checking if GnuPG is available at any of these paths: {format_as_paths(paths)}."
        )

    for path in paths:
        try:
            if verbose > 0:
                rich.print(f"Executing: \n> {path} --version")
            if verbose > 1:
                subprocess.run([path, "--version"], check=True, capture_output=False)
            else:
                subprocess.run([path, "--version"], check=True, capture_output=True)
            if verbose > 0:
                rich.print(f"Found GnuPG at {format_as_path(path)}.")
            return path
        except FileNotFoundError:
            if verbose > 0:
                print(f"Could not find GnuPG at {format_as_path(path)}.")

    # If here, it means that none of the paths worked
    paths = ", ".join([format_as_path(p) for p in paths])
    print(
        "[bold red]Error.[/bold red] "
        f"Could not find GnuPG (tried {paths}), make sure it is installed."
    )
    raise typer.Exit(code=1)


@app.command()
def backup(
    out: Annotated[
        Optional[Path],
        typer.Option(help="Output path with extension. If None, creates unique."),
    ] = None,
    encrypt: Annotated[bool, typer.Option(help="Encrypt backup.")] = True,
    gpg: Annotated[
        Optional[str],
        typer.Option(help="Path to GnuPG executable. If None, trying to guess."),
    ] = None,
    verbose: Annotated[
        int, typer.Option("--verbose", "-v", count=True, help="Be more verbose.")
    ] = 0,
):
    """
    Backup keys to a file.
    """
    gpg = get_gpg_command(gpg, verbose)

    if out is None:
        if verbose > 0:
            rich.print("No output file specified, using default.")
        out = get_default_out_path(encrypt)
    if verbose > 0:
        rich.print(f"Backing up to {format_as_path(out)}.")

    with TemporaryDirectory() as tmpdir:
        if verbose > 1:
            rich.print(f"Using temporary directory {format_as_path(tmpdir)}.")

        export_public_keys(tmpdir, verbose)
        export_private_keys(tmpdir, verbose)
        export_trust_database(tmpdir, verbose)
        compute_hashes(tmpdir, verbose)

        tar_path = create_tar(tmpdir, verbose)

        if encrypt:
            encrypt_file(tar_path)
            encrypted_tar_path = Path(f"{tar_path}.gpg")

            # `rename()` and `move()` do not work in all cases:
            # use `copy()` and `remove()`, but we are in a temporary context
            # manager, so no need to remove.
            #
            # https://stackoverflow.com/questions/42392600/oserror-errno-18-invalid-cross-device-link
            try:
                encrypted_tar_path.rename(out)
            except OSError:
                shutil.copy2(encrypted_tar_path, out)

            if verbose > 0:
                rich.print(
                    f"Encrypted and moved {format_as_path(tar_path)} to {format_as_path(out)}."
                )
        else:
            try:
                tar_path.rename(out)
            except OSError:
                shutil.copy2(tar_path, out)
            if verbose > 0:
                rich.print(
                    f"Moved {format_as_path(tar_path)} to {format_as_path(out)}."
                )

        rich.print(f"Backup complete. {format_as_path(out)} is ready.")


@app.command()
def restore(
    path: Annotated[Path, typer.Argument(help="Path to backup file.")],
    gpg: Annotated[
        Optional[str],
        typer.Option(help="Path to GnuPG executable. If None, trying to guess."),
    ] = None,
    verbose: Annotated[
        int, typer.Option("--verbose", "-v", count=True, help="Be more verbose.")
    ] = 0,
):
    """
    Restore keys from a file.
    """
    gpg = get_gpg_command(gpg, verbose)

    with TemporaryDirectory() as tmpdir:
        if verbose > 1:
            rich.print(f"Using temporary directory {format_as_path(tmpdir)}.")

        tmp_path = Path(tmpdir) / path.name
        shutil.copy2(path, tmp_path)
        path = tmp_path
        if verbose > 0:
            rich.print(f"Copied {format_as_path(path)} to {format_as_path(tmp_path)}.")

        if is_encrypted(path):
            path = decrypt(path, gpg)

        # Unpack the tarball
        if verbose > 0:
            rich.print(f"Unpacking {format_as_path(path)}.")
        path = unpack(path, tmpdir, verbose)

        check_hashes(path, verbose)
        import_keys(gpg, path=path / Path(PUBLIC_KEYS), verbose=verbose)
        import_keys(gpg, path=path / Path(PRIVATE_KEYS), verbose=verbose)
        import_trust_database(gpg, path=path / Path(OWNER_TRUST), verbose=verbose)
