import datetime
import os
import tarfile
from pathlib import Path
from subprocess import CompletedProcess

import rich

from gpg_backup.pretty import format_as_path
from gpg_backup.shell import run
from gpg_backup.constants import (
    PUBLIC_KEYS,
    PRIVATE_KEYS,
    OWNER_TRUST,
    HASHES,
    ALGO,
    EXTENSION_ENCRYPTED,
    EXTENSION_PLAIN,
)


def get_default_out_path(encrypt: bool) -> Path:
    now = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    if encrypt:
        return Path(f"gpg_backup_{now}.{EXTENSION_ENCRYPTED}")
    else:
        return Path(f"gpg_backup_{now}.{EXTENSION_PLAIN}")


def export_public_keys(tmpdir: str, verbose: int = 0) -> CompletedProcess:
    pub_path = os.path.join(tmpdir, PUBLIC_KEYS)
    if verbose > 0:
        rich.print(f"Exporting public keys to {format_as_path(pub_path)}.")

    cmd = ["gpg", "-a", "--export", "--export-options", "backup", "-o", pub_path]
    completed = run(cmd, verbose, error_msg="Could not export public keys.")

    return completed


def export_private_keys(tmpdir: str, verbose: int = 0) -> CompletedProcess:
    priv_path = os.path.join(tmpdir, PRIVATE_KEYS)
    if verbose > 0:
        rich.print(
            f"Exporting private keys to {format_as_path(priv_path)}. GnuPG might ask for your password(s)."
        )

    cmd = [
        "gpg",
        "-a",
        "--export-secret-keys",
        "--export-options",
        "backup",
        "-o",
        priv_path,
    ]
    completed = run(cmd, verbose, error_msg="Could not export private keys.")

    return completed


def export_trust_database(tmpdir: str, verbose: int = 0) -> CompletedProcess:
    trust_path = Path(tmpdir) / Path(OWNER_TRUST)
    if verbose > 0:
        rich.print(f"Exporting trust database to {format_as_path(trust_path)}.")

    # `gpg --export-ownertrust` does not support the `-o` option, so we
    # capture the stdout and then write to a file
    cmd = ["gpg", "--export-ownertrust"]
    completed = run(cmd, verbose)

    with open(trust_path, "w") as f:
        f.write(completed.stdout.decode())

    return completed


def compute_hashes(tmpdir: str, verbose: int = 0) -> CompletedProcess:
    if verbose > 0:
        print("Computing hashes of individual files.")

    cmd = f"md5sum * > {HASHES}"
    completed = run(
        cmd,
        verbose,
        error_msg="Could not compute hashes of individual files.",
        shell=True,
        cwd=tmpdir,
    )

    return completed


def create_tar(tmpdir: str, verbose: int = 0, filename: str = "export.tar") -> Path:
    pwd = os.getcwd()
    os.chdir(tmpdir)

    with tarfile.TarFile(name=filename, mode="w") as archive:
        if verbose > 0:
            rich.print(f"Created (empty) archive {format_as_path(filename)}.")
        if Path(PUBLIC_KEYS).exists():
            archive.add(PUBLIC_KEYS)
            if verbose > 0:
                rich.print(f"Added {format_as_path(PUBLIC_KEYS)} to archive.")
        else:
            if verbose > 0:
                rich.print(
                    f"File {format_as_path(PUBLIC_KEYS)} does not exist."
                    "No public keys were present in the keychain?"
                )
        if Path(PRIVATE_KEYS).exists():
            archive.add(PRIVATE_KEYS)
            if verbose > 0:
                rich.print(f"Added {format_as_path(PRIVATE_KEYS)} to archive.")
        else:
            if verbose > 0:
                rich.print(
                    f"File {format_as_path(PRIVATE_KEYS)} does not exist."
                    "No private keys were present in the keychain?"
                )

        # Do no check `OWNER_TRUST` and `HASHES` for existence, as they are
        # always created by the script.
        archive.add(OWNER_TRUST)
        if verbose > 0:
            rich.print(f"Added {format_as_path(OWNER_TRUST)} to archive.")
        archive.add(HASHES)
        if verbose > 0:
            rich.print(f"Added {format_as_path(HASHES)} to archive.")

    os.chdir(pwd)
    return Path(tmpdir) / Path(filename)


def encrypt_file(filepath: str | Path) -> CompletedProcess:
    """
    Encrypts a file using GnuPG.
    """
    cmd = ["gpg", "--cipher-algo", ALGO, "--symmetric", str(filepath)]
    completed = run(cmd, check=True)

    return completed
