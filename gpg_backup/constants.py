PUBLIC_KEYS = "pubkeys.asc"
PRIVATE_KEYS = "privkeys.asc"
OWNER_TRUST = "otrust.txt"
HASHES = "md5sums.txt"
ALGO = "AES256"

EXTENSION_ENCRYPTED = ".tar.gpg"
EXTENSION_PLAIN = ".tar"
