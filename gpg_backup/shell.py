import subprocess
from typing import List
import typer
import rich
from gpg_backup.pretty import format_as_path


def run(
    cmd: str | List[str], verbose: int = 0, error_msg: str = "", **kwargs
) -> subprocess.CompletedProcess:
    # For debugging and verbose outputs
    if isinstance(cmd, list):
        cmd_str = " ".join(cmd)
    else:
        cmd_str = cmd

    if verbose > 1:
        if "cwd" in kwargs:
            cwd = kwargs["cwd"]
            rich.print(f"Executing (in {format_as_path(cwd)}):\n> {cmd_str}")
        else:
            rich.print(f"Executing: \n> {cmd_str}")

    # Execute command
    kwargs["check"] = False
    kwargs["capture_output"] = True
    completed = subprocess.run(cmd, **kwargs)

    # Handle errors
    if completed.returncode != 0:
        rich.print(
            "[bold red]Error.[/bold red] "
            f"{error_msg}" if len(error_msg) > 0 else ""
            f"\nThe following command failed with return code {completed.returncode}:\n> "
            f"{cmd_str}\n"
            f"{completed.stderr.decode()}"
        )
        raise typer.Exit(code=1)

    # Show output
    if verbose > 0:
        # STDERR might contains warnings even if the command succeeded
        stderr = completed.stderr.decode()
        if len(stderr) > 0:
            typer.echo(stderr)
    if verbose > 1:
        stdout = completed.stdout.decode()
        if len(stdout) > 0:
            typer.echo(stdout)

    return completed
