from pathlib import Path
from subprocess import CompletedProcess
from tarfile import ReadError, TarFile
import os
import rich

from gpg_backup.constants import (
    EXTENSION_ENCRYPTED,
    EXTENSION_PLAIN,
    HASHES,
)
from gpg_backup.pretty import format_as_path
from gpg_backup.shell import run


def is_encrypted(path: str | Path) -> bool:
    path = Path(path)

    if path.name.endswith(EXTENSION_ENCRYPTED):
        return True
    elif path.name.endswith(EXTENSION_PLAIN):
        return False
    else:
        raise ValueError(
            f"The file {format_as_path(path)} does not have a recognized extension."
        )


def decrypt(
    path: str | Path,
    gpg: str,
    verbose: int = 0,
) -> Path:
    """
    Decrypts a file using GnuPG.
    """
    path = Path(path)

    # Drop .gpg from [stem].tar.gpg
    if path.suffix != ".gpg":
        raise ValueError(
            f"The file {format_as_path(path)} does not have the expected .gpg extension."
        )
    out = path.with_suffix("")

    if verbose > 0:
        rich.print(
            f"Decrypting {format_as_path(path)} to {format_as_path(out)} using GnuPG."
        )

    run([gpg, "--decrypt", "--output", str(out), str(path)], verbose=verbose)
    return out


def unpack(path: str | Path, tmpdir: str | Path, verbose: int = 0) -> Path:
    """
    Unpacks a tar file.
    """
    tmpdir = Path(tmpdir)
    # Remove .tar from [stem].tar
    unpacked_path = tmpdir / path.with_suffix("")
    if verbose > 0:
        rich.print(
            f"Unpacking {format_as_path(path)} to {format_as_path(unpacked_path)}."
        )
    try:
        TarFile.open(path, "r").extractall(unpacked_path)
    except ReadError:
        raise ValueError(
            f"The file {format_as_path(path)} is not a valid tar file."
        )
    if verbose > 1:
        # Print contents of unpacked_path
        rich.print("Unpacked tarball contents:")
        rich.print(f"{format_as_path(unpacked_path)}")
        n = len(unpacked_path.parts)
        for i, p in enumerate(unpacked_path.iterdir()):
            if i == (n - 1):
                rich.print(f"└── {p}")
            else:
                rich.print(f"├── {p}")
    return unpacked_path


def check_hashes(path: str | Path, verbose: int = 0) -> CompletedProcess:
    """
    Check the hashes of the files in the tarball.
    """
    path = Path(path)
    cmd = ["md5sum", "-c", HASHES]
    completed = run(cmd, verbose, error_msg="Hashes do not match.", cwd=str(path))
    return completed


def import_keys(
    gpg: str, path: str | Path = None, verbose: int = 0
) -> CompletedProcess:
    """
    Import public keys from a file.
    """
    path = Path(path)
    if verbose > 0:
        rich.print(f"Importing public keys from {format_as_path(path)}.")

    cmd = [gpg, "--import", "--import-options", "restore", str(path)]
    completed = run(cmd, verbose, error_msg="Could not import public keys.")
    return completed


def import_trust_database(
    gpg: str, path: str | Path = None, verbose: int = 0
) -> CompletedProcess:
    """
    Import public keys from a file.
    """
    path = Path(path)
    if verbose > 0:
        rich.print(f"Importing public keys from {format_as_path(path)}.")

    cmd = [gpg, "--import-ownertrust", "--import-options", "restore", str(path)]
    completed = run(cmd, verbose, error_msg="Could not import public keys.")
    return completed