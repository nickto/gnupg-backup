from typing import List


def format_as_path(text: str) -> str:
    return f"[magenta]{text}[/magenta]"


def format_as_paths(texts: List[str]) -> str:
    formatted = []
    for text in texts:
        formatted.append(f"[magenta]{text}[/magenta]")
    return ", ".join(formatted)
